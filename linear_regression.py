import numpy as np
from sklearn.datasets import make_regression
from sklearn.metrics import r2_score
import matplotlib.pyplot as plt
import pandas as pd


def train_test_split(X, y, test_size):
    len = X.shape[0]
    train_size = 1 - (1 * test_size)
    X_train = X[0: int(train_size * len), :]
    y_train = y[0: int(train_size * len)]
    X_test = X[int(train_size * len):, :]
    y_test = y[int(train_size * len):]
    return X_train, y_train, X_test, y_test


X, y = make_regression(n_samples=1000, n_features=2, n_targets=1, noise=0)
X_train, y_train, X_test, y_test = train_test_split(X, y, 0.2)

# Linear Regression
eta = 0.001
epoch = 100
w = np.array([0.0, 0.0]).reshape(-1, 1)
bias = 0
for e in range(epoch):
    for idx, ip in enumerate(X_train):
        ip = ip.reshape(-1, 1)
        target = y_train[idx]
        pred = w.T @ ip + bias
        pred = pred.reshape(-1)
        pred = pred[0]
        gradient = (pred - target)
        w -= eta * gradient * ip
        bias -= eta * gradient
y_pred = []
for idx, ip in enumerate(X_test):
    pred = w.T @ ip + bias
    y_pred.append(pred[0])

df = pd.DataFrame({"Pred": y_pred, "Target": y_test},
                  index=range(y_test.shape[0]))
print(df.head(10))
accuracy_score = r2_score(y_test, y_pred)
print("Accuracy Score: ", accuracy_score)
